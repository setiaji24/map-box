//import "mapbox-gl/dist/mapbox-gl.css";
//import "react-map-gl-geocoder/dist/mapbox-gl-geocoder.css";
import React, { useState, useRef } from 'react';
import MapGL, {Marker} from "react-map-gl";
import { GeoJsonLayer } from "deck.gl";
import Geocoder from "react-map-gl-geocoder";
import { Card as CardBox, Grid, Button, Container, Divider, Form, Radio, Modal, Input, TextArea } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import './App.css';
import Pin from './pin';
import styles from './index.scss';

const PENCIL_IMAGE = 'https://s3-ap-southeast-1.amazonaws.com/cdn.vizitrip.com/assets/images/trip-planner/icons/pencil.svg';
const CLOSE_IMAGE = 'https://s3-ap-southeast-1.amazonaws.com/cdn.vizitrip.com/assets/images/trip-planner/icons/close.svg';

const Card = (props) => {
  return (
    <CardBox className={styles.container}>
      <CardBox.Content>
        {props.children}
      </CardBox.Content>
    </CardBox>
  )
}

const MAPBOX_TOKEN = "XXXXXXXXXXXXXXXXX"; //fill with your map box token.

function CurrentLocation(){
  const location = window.navigator && window.navigator.geolocation;
  const [latitudeFirst, setLatitudeFirst] = useState("");
  const [longitudeFirst, setLongitudeFirst] = useState("");
  if (location) {
    location.getCurrentPosition((position) => {
      if(position.coords.latitude != 0 && position.coords.longitude != 0){
        setLatitudeFirst(position.coords.latitude);
        setLongitudeFirst(position.coords.longitude);
      }
    });
  }    
  return(
    <div>
    {latitudeFirst != 0 && longitudeFirst != 0 ? 
      <ViziMap latitudeFirst={latitudeFirst} longitudeFirst={longitudeFirst} />
      :null
    } 
    </div>
  )
}


function ViziMap(props){
  const [open, setOpen] = useState(false);
  const [dimmer, setDimmer] = useState(true);
  const [contentModal, setContentModal] = useState('');
  const [resultContent, setResultContent] = useState([]);
  const [toggleEdit, setToggleEdit] = useState(false);
  const nameRef = useRef(null);
  const placeRef = useRef(null);
  const [isEdit, setIsEdit] = useState(true);
  const [togglePlace, setTogglePlace] = useState(false);

  const [searchResultLayer, setSearchResultLayer] = useState(null);
  const [searchResult, setSearchResult] = useState({});
  const [mapRef, setMapRef] = useState(React.createRef());
  const [scrollZoom, setScrollZoom] = useState(false);
  const [viewport, setViewport] = useState({
    latitude: props.latitudeFirst,
    longitude: props.longitudeFirst,
    zoom: 14,
    bearing: 0,
    pitch: 0
  });
  const [events, setEvents] = useState({});

  function handleClick(e) {
    e.preventDefault();
    setOpen(true);
    setDimmer(true);
  }

  function handleClickEdit(e) {
    setTimeout(() => {
      setToggleEdit(true);
      nameRef.current.focus();
      setIsEdit(false);
    }, 500);
  }
  
  function handleBlurEdit(e) {
    setIsEdit(true);
    setToggleEdit(false);
  }
  
  function handleClose(e) {
    e.preventDefault();
    setOpen(false);
  }
  
  function handleSubmit(e) {
    e.preventDefault();
  }

  
  function handleViewportChange(viewport) {
    setViewport({viewport, ...viewport});
  }

  function _updateViewport(viewport) {
    setViewport({viewport, ...viewport});
  };

  function handleGeocoderViewportChange(viewport) {
    const geocoderDefaultOverrides = { transitionDuration: 1000 };
    return handleViewportChange({
      ...viewport,
      ...geocoderDefaultOverrides
    });
  }

  function handleOnResult(event) {
    setSearchResultLayer(
      new GeoJsonLayer({
        id: "search-result",
        data: event.result.geometry,
        getRadius: 1000,
        pointRadiusMinPixels: 10,
        pointRadiusMaxPixels: 10
      })
    );
    console.log("###event.result: ", event.result);
    let dataResult = {
      longitude: event.result.geometry.coordinates[0],
      latitude: event.result.geometry.coordinates[1],
      place_name: event.result.place_name
    }
    setSearchResult(dataResult);
  }

  function _logDragEvent(name, event) {
    setEvents({
      events: {
        ...events,
        [name]: event.lngLat
      }
    });
  }

  function _onMarkerDragStart(event) {
    _logDragEvent('onDragStart', event);
  };

  function _onMarkerDrag(event) {
    _logDragEvent('onDrag', event);
  };

  function _onMarkerDragEnd(event) {
    _logDragEvent('onDragEnd', event);
    setViewport({
      ...viewport, longitude: event.lngLat[0], latitude: event.lngLat[1]
    });
    //console.log("###viewport: ", viewport);
    let dataResult = {
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
      place_name: null
    }
    setSearchResult(dataResult);
    setTogglePlace(true);
    placeRef.current.focus();

  };
  

  return (
    <div>

    <Card>
    <Container>
    {/* onClick={handleClick.bind(data)} */}
    <div className={styles.extra_title} onClick={handleClick}>
    Ada tujuan lain / kunjungan lain yang tidak ada di sini ?
    </div>

    <Modal dimmer={dimmer} open={open} onClose={handleClose}>
    <Modal.Content>
    <Modal.Description>

    <Form className={styles.form_container} onSubmit={handleSubmit}>
      <div className={styles.close} style={{float: 'right', position: 'relative'}}>
        <img src={CLOSE_IMAGE} onClick={handleClose} style={{cursor: 'pointer'}} ></img>
      </div>
      <Form.Field>
        <div className={styles.row_name} style={{textAlign: 'center'}}>
        {toggleEdit ?
          <input className={styles.input_text} placeholder='Nama Destinasi' ref={nameRef} onBlur={handleBlurEdit} />
        :
          <span className={styles.item_name}>Nama Destinasi</span>
        }
        {isEdit ?
            <img src={PENCIL_IMAGE} onClick={handleClickEdit}></img>
          : null}
        </div>
      </Form.Field>
      <div className={styles.description_title}>Deskripsi :</div>
      <textarea className={styles.description_text} placeholder='Tulis nama tempat disini'></textarea>
      <div className={styles.description_title} style={{paddingTop: '10px', paddingBottom: '10px'}}>Tag Lokasi :</div>

      <div style={{paddingTop: '10px', paddingBottom: '70px'}}>Place Name :
        <input className={styles.input_text} placeholder='Place name' ref={placeRef} style={{display: 'none'}} />
      </div>

      {/* <div style={{ height: "25vh" }} id='map'></div> */}
      
      <div style={{ height: "25vh" }}>
        <MapGL
          ref={mapRef}
          {...viewport}
          width="100%"
          height="100%"
          mapStyle="mapbox://styles/mapbox/streets-v11"
          scrollZoom={scrollZoom}
          onViewportChange={_updateViewport}
          mapboxApiAccessToken={MAPBOX_TOKEN}
        >
          <Marker
            longitude={viewport.longitude}
            latitude={viewport.latitude}
            offsetTop={-20}
            offsetLeft={-10}
            draggable
            onDragStart={_onMarkerDragStart}
            onDrag={_onMarkerDrag}
            onDragEnd={_onMarkerDragEnd}
          >
            <Pin size={40} />
          </Marker>

          <Geocoder
            mapRef={mapRef}
            onResult={handleOnResult}
            onViewportChange={handleGeocoderViewportChange}
            mapboxApiAccessToken={MAPBOX_TOKEN}
            position="top-left"
            placeholder="Tulis langsung detail alamat"
          />
        </MapGL>
      </div>

      <div className={styles.address_title} style={{paddingTop: '10px', paddingBottom: '10px'}}>Alamat :</div>
      <textarea className={styles.address_text} placeholder='Tulis langsung detail alamat'></textarea>
      <p style={{color:'#2f7fbe', cursor: 'pointer', textAlign: 'center', fontWeight: 'bold'}}>Tambahkan</p>
    </Form>

    </Modal.Description>
    </Modal.Content>
    </Modal>
    </Container>
    </Card>

    </div>
  );
}

export default CurrentLocation;
